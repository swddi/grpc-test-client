<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace Swddi\Grpc;

use Hyperf\HttpServer\Router\DispatcherFactory;
use Hyperf\HttpServer\Router\Handler;
use Hyperf\Utils\Str;
use Psr\Container\ContainerInterface;
use ReflectionClass;

class AutoControllerManager
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ReflectionClass[]
     */
    private array $refmap = [];

    /**
     * @var array 路由数据
     */
    public array $routeData = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @throws \ReflectionException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function run()
    {
        $factory = $this->container->get(DispatcherFactory::class);
        $router = $factory->getRouter('grpc');
        [$staticRouters, $variableRouters] = $router->getData();
        $routeData = [];
        foreach ($staticRouters as $method => $item) {
            /** @var  Handler $route */
            foreach ($item as $route) {
                //init data
                $tmp = [
                    'method' => $method,
                    'route' => $route->route,
                ];

                //grpc controller
                if (is_string($route->callback)) {
                    if (strpos($route->callback, '@')) {
                        $callbackArr = explode("@", $route->callback);
                    } elseif (strpos($route->callback, '::')) {
                        $callbackArr = explode("::", $route->callback);
                    } else {
                        continue;
                    }
                } else {
                    $callbackArr = $route->callback;
                }
                $reflectionClass = new \ReflectionClass($callbackArr[0]);
                if (!$reflectionClass->hasMethod($callbackArr[1])) {
                    continue;
                }
                $tmp['grpc_controller'] = $callbackArr[0];
                $tmp['grpc_action'] = $callbackArr[1];

                //grpc：resp proto
                $reflectionMethod = $reflectionClass->getMethod($callbackArr[1]);
                $responseClass = $reflectionMethod->getReturnType();
                if ($responseClass === null) {
                    continue;
                }
                $tmp['response'] = $reflectionClass->name;

                //grpc：req proto
                $reflectionParameters = $reflectionMethod->getParameters();
                if (empty($reflectionParameters)) {
                    return [];
                }
                $requestClass = $reflectionParameters[0]->getClass()->name;
                $tmp['request'] = $requestClass;

                //grpc:req protoClass
                $classProperties = $this->getProtoClassProperties($requestClass);
                $tmp['params'] = $classProperties['properties'];

                //add
                $routeData[] = $tmp;
            }
        }
        $this->routeData = $routeData;
    }

    /**
     * 获取对象的所有属性
     *
     * @param $className
     * @param array $registeredClass
     * @return array
     * @throws \ReflectionException
     */
    private function getProtoClassProperties($className, &$registeredClass = []): array
    {
        //init
        $data['class'] = $className;

        //验证是否是枚举类，因为枚举类没有构造函数
        $reflectionClass = $this->getReflection($className);
        if (!$reflectionClass->hasMethod('__construct')) {
            $const = $reflectionClass->getStaticPropertyValue('valueToName');
            $data['enum'] = $const;
        } else {
            $properties = [];
            //获取本对象的所有set方法
            $setterActions = $this->getSetterAction($className);
            //遍历set方法，根据doc获取属性类型
            foreach ($setterActions as $fieldName => $action) {
                $methodDocument = $this->getMethodDocument($className, $action);
                $properties[] = $this->parseMethodDocument($fieldName, $methodDocument, $registeredClass);
            }
            $data['properties'] = $properties;
        }
        return $data;
    }

    /**
     * [
     *  'name'=>属性字段名
     *  'multi'=>bool 视为为数组属性
     *  'data'= 数据类型
     * ]
     *
     * @param string $fieldName
     * @param array $data
     * @return array
     * @throws \ReflectionException
     */
    private function parseMethodDocument(string $fieldName, array $data, &$registeredClass): array
    {
        $property = [];
        $property['name'] = $fieldName;
        foreach ($data as $item) {
            if (strpos($item, 'Generated from protobuf field') !== false) {
                $protobufDoc = $item;
            }
        }
        //proto 命名空间
        $protoTYpe = str_replace(' ', '', Str::between($protobufDoc, '<code>', $fieldName));

        //处理map
        if (strpos($protoTYpe, 'map') !== false) {
            $mapArr = explode(',', str_replace(['<', '>', 'map'], '', $protoTYpe));
            $protobufToClassName = $this->protobufToClassName($mapArr[1], $registeredClass);
            $property['map'][$mapArr[0]] = $protobufToClassName;
        }

        //处理repeated属性
        if (strpos($protoTYpe, 'repeated') !== false) {
            $protoTYpe = str_replace('repeated', '', $protoTYpe);
            $protobufToClassName = $this->protobufToClassName($protoTYpe, $registeredClass);
            $property['repeated'] = $protobufToClassName;
        }

        //处理optional属性
        if (strpos($protoTYpe, 'optional') !== false) {
            $protoTYpe = str_replace('optional', '', $protoTYpe);
            $protobufToClassName = $this->protobufToClassName($protoTYpe, $registeredClass);
            $property['optional'] = $protobufToClassName;
        }
        return $property;
    }

    /**
     * proto命名空间转class
     *
     * @param string $protobuf
     * @param $registeredClass
     * @return mixed
     * @throws \ReflectionException
     */
    private function protobufToClassName(string $protobuf, &$registeredClass)
    {
        if (strpos($protobuf, '.') === false) {
            return $protobuf;
        }
        $className = '';
        $explode = explode('.', $protobuf);
        foreach ($explode as $item) {
            $className .= '\\' . ucfirst($item);
        }
        $className = str_replace('\\\\', '\\', $className);
        if (in_array($className, $registeredClass)) {
            return $className;
        }
        $registeredClass[] = $className;
        return $this->getProtoClassProperties($className, $registeredClass);
    }

    /**
     * 根据request proto对象的构造函数获取所有属性设置函数
     *
     * @param $className
     * @return array
     * @throws \ReflectionException
     */
    private function getSetterAction($className): array
    {
        $methodDocument = $this->getMethodDocument($className, '__construct');
        $properties = [];
        if (empty($methodDocument)) {
            return [];
        }
        foreach ($methodDocument as $item) {
            if (strpos($item, '@type') === false) {
                continue;
            }
            $lineArr = array_values(array_filter(explode(' ', $item)));
            $lineArr = array_values($lineArr);
            //属性名
            $name = str_replace('$', '', $lineArr[3]);
            $properties[$name] = $this->actionTrance($name);
        }
        return $properties;
    }

    /**
     * 获取反射对象
     *
     * @param $class
     * @return ReflectionClass
     * @throws \ReflectionException
     */
    private function getReflection($class): ReflectionClass
    {
        if (!empty($this->refmap[$class])) {
            $reflectionClass = $this->refmap[$class];
        } else {
            $reflectionClass = new \ReflectionClass($class);
        }
        return $reflectionClass;
    }

    /**
     * 获取指定方法的doc
     *
     * @param $class
     * @param $method
     * @return array
     * @throws \ReflectionException
     */
    private function getMethodDocument($class, $method): array
    {
        $reflectionClass = $this->getReflection($class);
        $reflectionMethod = $reflectionClass->getMethod($method);
        $docComment = $reflectionMethod->getDocComment();
        return explode(PHP_EOL, $docComment);
    }

    /**
     * 属性名转set方法
     *
     * @param $name
     * @return string
     */
    private function actionTrance($name): string
    {
        $explode = explode('_', $name);
        $action = 'set';
        foreach ($explode as $item) {
            $action .= ucfirst($item);
        }
        return $action;
    }
}

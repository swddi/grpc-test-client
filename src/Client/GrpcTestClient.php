<?php
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Swddi\Grpc\Client;

class GrpcTestClient extends \Hyperf\GrpcClient\BaseClient
{
    public function sendTest($uri, $args, $returnClass)
    {
        return $this->_simpleRequest($uri, $args, [$returnClass, 'decode']);
    }
}
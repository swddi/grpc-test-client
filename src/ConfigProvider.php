<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Swddi\Grpc;

use Hyperf\Server\Event;
use Hyperf\Server\Server;
use Swddi\Grpc\Listener\BeforeMainServerStartListener;
use Swddi\Grpc\Server\SwddiHttpTestServer;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'server' => [
                'servers' => [
                    [
                        'name' => 'grpc-helper-server',
                        'type' => Server::SERVER_HTTP,
                        'host' => '0.0.0.0',
                        'port' => (int)env('LOCAL_RPC_TEST_CLIENT_PORT_V2', 4480),
                        'sock_type' => SWOOLE_SOCK_TCP,
                        'callbacks' => [
                            Event::ON_REQUEST => [SwddiHttpTestServer::class, 'onRequest'],
                        ],
                    ],
                ],
            ],
            'listeners' => [
                BeforeMainServerStartListener::class,
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
        ];
    }
}

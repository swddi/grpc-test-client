# grpc_test_client

#### 介绍
供开发环境使用,能快速调试当前服务的grpc接口

#### 安装教程

```composer
"require-dev": {
"swddi/grpc-test-client": "dev-master"
}
```

#### 使用说明

1. 开发环境启动服务自动开启端口为4480的http-server,如果本地docker环境,记得开放4480的端口映射.
2. http://localhost:4480/swddi/grpc/test 访问grpc的测试客户端
3. 测试客户端为了直观显示,protobuf对象返回以json展示.
4. 在env 中可以自定义页面的端口号LOCAL_RPC_TEST_CLIENT_PORT_V2
5. 必须在env 中配置 GRPC_LISTEN_PORT 这个端口为你启动grpc服务的端口，如果不配置，本插件将无法转发请求
6. 本插件是通过反射获取protobuf的返回对象,所以写代码时,Controller 必须声明方法返回参数类型
7. optional属性,支持普通类型（string,int）、无嵌套的对象
8. map仅支持普通类型（string、int）
9. repeated可以使用无嵌套的对象

```php
//这个是hyperf官方的写法.这样写是在本插件中不会被识别到
public function sayHello(HiUser $user) 
{
    $message = new HiReply();
    $message->setMessage("Hello World");
    $message->setUser($user);
    return $message;
}
//正确的写法
public function sayHello(HiUser $user) :HiReply
{
    $message = new HiReply();
    $message->setMessage("Hello World");
    $message->setUser($user);
    return $message;
}
```